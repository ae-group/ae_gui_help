<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# gui_help 0.3.55

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_gui_help/develop?logo=python)](
    https://gitlab.com/ae-group/ae_gui_help)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_gui_help/release0.3.54?logo=python)](
    https://gitlab.com/ae-group/ae_gui_help/-/tree/release0.3.54)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_gui_help)](
    https://pypi.org/project/ae-gui-help/#history)

>ae_gui_help package 0.3.55.

[![Coverage](https://ae-group.gitlab.io/ae_gui_help/coverage.svg)](
    https://ae-group.gitlab.io/ae_gui_help/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_gui_help/mypy.svg)](
    https://ae-group.gitlab.io/ae_gui_help/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_gui_help/pylint.svg)](
    https://ae-group.gitlab.io/ae_gui_help/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_gui_help)](
    https://gitlab.com/ae-group/ae_gui_help/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_gui_help)](
    https://gitlab.com/ae-group/ae_gui_help/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_gui_help)](
    https://gitlab.com/ae-group/ae_gui_help/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_gui_help)](
    https://pypi.org/project/ae-gui-help/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_gui_help)](
    https://gitlab.com/ae-group/ae_gui_help/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_gui_help)](
    https://libraries.io/pypi/ae-gui-help)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_gui_help)](
    https://pypi.org/project/ae-gui-help/#files)


## installation


execute the following command to install the
ae.gui_help package
in the currently active virtual environment:
 
```shell script
pip install ae-gui-help
```

if you want to contribute to this portion then first fork
[the ae_gui_help repository at GitLab](
https://gitlab.com/ae-group/ae_gui_help "ae.gui_help code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_gui_help):

```shell script
pip install -e .[dev]
```

the last command will install this package portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_gui_help/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.gui_help.html
"ae_gui_help documentation").
