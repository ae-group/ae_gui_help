{
"": """\
   [b]interaktiver hilfemodus[/b]

alle bildschirm-elemente sind
deaktiviert.

* der erste tipper auf ein
   bildschirm-element zeigt
   den zugehörigen hilfetext
   und reaktiviert es.

* beim zweiten tip wird die
   jeweilige funktion ganz
   normal ausgeführt.

tippe auf das fragezeichen symbol
([b]?[/b]) links oben um den
hilfemodus wieder zu verlassen.""",
#==================================
'help_app_state#font_size': """\
schriftgröße ändern

aktuell: [b]{round(self.value)}[/b]px""",
#----------------------------------
'help_app_state#pending_requests_frequency': """\
aktualisierungsfrequenz der
fortschrittsanzeige und grafik

aktuell: [b]{round(self.value, 1)}[/b]Hz""",
#----------------------------------
'help_app_state#sound_volume': """\
lautstärke ändern.

tippe ganz links,
um alle akustischen
signale zu unterdrücken.

aktuelle lautstärke: [b]{round(self.value * 100)}[/b]%""",
#----------------------------------
'help_app_state#vibration_volume': """\
intensität vibrationen

tippe ganz links, um
alle vibrationen zu
unterdrücken.

aktuelle vibration: [b]{round(self.value * 100)}[/b]%""",
#==================================
'help_flow#change_file_chooser_path': """\
dateien dieses
ordners anzeigen""",
#----------------------------------
'help_flow#change_lang_code': """\
sprache der app
auf [b]'{main_app.get_txt_(self.lang_code)}'[/b]
ändern""",
#----------------------------------
'help_flow#change_light_theme': """\
hintergrund-farbe
der app zwischen
[b]'hell'[/b] oder
[b]'dunkel'[/b]
umschalten""",
#----------------------------------
'help_flow#close_popup': "fenster schließen",
#----------------------------------
'help_flow#open_color_editor':
    {'': """\
farben-editor anzeigen.

hier kann die farbe
geändert werden, welche
für [b]{dict(
      create_ink='das angelegen',
      delete_ink='das löschen',
      error_ink='fehler',
      flow_id_ink='fokussiertes',
      flow_path_ink='das navigieren',
      info_ink='infos',
      read_ink='das lesen',
      selected_ink='ausgewähltes',
      unselected_ink='abgewähltes',
      update_ink='das bearbeiten',
      warn_ink='warnungen',
    ).get(self.color_name)}[/b]
verwendet wird.""",
    'after': """\
tippe zweimal schnell
nacheinander auf den
farbkreis oder benutze
die schieberegler um
die farbe zu ändern.

tippe nochmals auf den
knopf, um den editor
wieder zu schließen.""", },
#----------------------------------
'help_flow#open_iterable_displayer':
    {'': "details anzuzeigen",
     'next_help_id': 'help_flow#close_popup', },
#----------------------------------
'help_flow#open_user_preferences':
    {'': """\
dieser zahnrad-knopf dient zum
öffnen der benutzer-einstellungen.""",
    'after': """\
die nun angezeigten optionen
dienen zur konfiguration der
app, zum beispiel um farben
und schriftgröße anzupassen.

tippe auf eine der optionen,
um weitere hilfe zu erhalten.""", },
#----------------------------------
'help_flow#select_file_chooser_path': """\
ordner-schnellauswahl
anzeigen""",
#----------------------------------
'help_flow#toggle_file_chooser_view': """\
umschalter zwischen
listen- und symbol-
ansicht.

tippe zum umschalten
auf [b]{'listen' if self.state == 'down' else 'symbol'}-ansicht[/b]""",
#==================================
'tour_page#':
    {'title_text': """
¡willkommen! zur
einweisungstour
""",
     'page_text': """\
der ovale [b]{get_text('next')}[/b] knopf rechts unterhalb
wechselt zur nächsten tourseite.

der rote knopf mit dem [b]X[/b] bricht die tour ab.

diese tour erscheint nicht mehr beim
start der app, sobald alle seiten
vollständig durchlaufen wurden.

tippe nun auf den [b]{get_text('next')}[/b] knopf um
zur nächsten tourseite zu gelangen.
""", },
#----------------------------------
'tour_page#page_switching':
    {'title_text': "gut gemacht!",
     'page_text': """\
manche app tours wechseln automatisch
zur nächsten seite.

zurück zur vorherigen seite geht's mit
dem [b]{get_text('back')}[/b] knopf.""", },
#----------------------------------
'tour_page#responsible_layout':
    {'title_text': "selbst-anpassendes layout",
     'page_text': """\
das layout dieser app passt sich flexibel
an die auflösung und ausrichtung des
{"bildschirms" if os_platform in ('ios', 'android') else "fensters"} an.

zusätzlich werden separate layouts für
hoch- und querformat bereitgestellt.

gerade wird das [b]{'quer' if app.landscape else 'hoch'}format[/b] layout
angezeigt.

{'kippe nun den bildschirm' if os_platform in ('android', 'ios') else 'verändere nun die fenstergröße'}
""", },
#----------------------------------
'tour_page#tip_help_intro':
    {'title_text': "hilfe auf tip",
     'tip_text': """\
normalerweise zeigt dieses tooltip fenster den hilfetext zu einem knopf an.
der pfeil am rand des tooltips verweist auf den jeweils beschriebenen knopf.

der gerade vom pfeil anvisierte knopf aktiviert die tip hilfe. ein weiterer
tip auf diesen knopf deaktiviert den hilfemodus wieder. zudem kann mit
diesem knopf eine app tour abgebrochen werden.""",
     'page_text': """\
bei aktiviertem hilfemodus sind alle
knöpfe deaktiviert und blau markiert.

nach dem ersten tip auf einen knopf
wird dessen hilfetext angezeigt.

ein weiterer tip auf den knopf führt
dann die hinterlegte funktion aus.""", },
#----------------------------------
'tour_page#tip_help_tooltip':
    {'title_text': "tooltip",
     'page_text': """\
das tooltip fenster zeigt auf den knopf der
das menü der benutzer-einstellungen öffnet.

ein tip auf den pfeil des tooltip fensters
macht dieses transparent und erlaubt das
tippen von dahinter versteckten knöpfen.

in manchen hilfetexten wird in der unteren
rechten ecke des tooltip-fensters ein
fragezeichen mit einem kreis angezeigt.
ein tipper auf diesen kreis startet eine
tour welche die jeweilige funktion noch
ausführlicher erklärt.""", },
#----------------------------------
'tour_page#layout_font_size':
    {'fade_out_app': 0.159,
     'page_text': """\
obwohl diese app mit unterschiedlichsten
bildschirm-auflösungen zurechtkommt,
kann es manchmal passieren das
bildschirm-elemente sich überlappen oder
nicht vollständig angezeigt werden.

falls das {'kippen des geräts' if os_platform in ('android', 'ios') else 'vergrößern des fensters'} nicht ausreicht
kann die schriftgröße mit dem gerade
hervorgehobenen schieberegler verkleinert
werden.""", },
#----------------------------------
'tour_page#tour_end':
    {'title_text': "¡glückwunsch!",
     'page_text': """\
die einweisungstour ist am ende angelangt.


gib im nachfolgenden popup-fenster deinen
ruf-/spitznamen ein um diese tour nicht
mehr beim app start anzuzeigen.""", },
#==================================
"create_ink": "anlegen{'/hinzufügen/registrieren' if app.landscape else ''} farbe",
"delete_ink": "löschen farbe",
"flow_id_ink": "{'farbe des fokussierten' if app.landscape else 'fokussierter'} eintrag",
"flow_path_ink": "{'farbe der ' if app.landscape else ''}listen-navigation",
"error_ink": "fehler farbe",
"info_ink": "info farbe",
"read_ink": "lesen farbe",
"selected_ink": "{'farbe der ausgewählten' if app.landscape else 'ausgewählte'} einträge",
"start app onboarding tour": "einweisungstour starten",
"stop app onboarding tour": "einweisungstour stoppen",
"unselected_ink": "{'farbe der abgewählten' if app.landscape else 'abgewählte'} einträge",
"update_ink": "bearbeiten farbe",
"warn_ink": "warnung farbe",
}
